; bed.g
; called to perform automatic bed compensation via G32
;
; generated by RepRapFirmware Configuration Tool v2.1.8 on Thu Feb 27 2020 07:34:28 GMT+0000 (Greenwich Mean Time)
M561 ; clear any bed transform
M401 ; Deploy probe
G29  ; probe the bed and enable compensation
M402 ; Retract probe

