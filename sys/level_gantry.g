; Adjust the Z motors to level the gantry
G90                          ; Absolute positions
G1  X10 Y30 F10000
G30 P0 X10  Y10  Z-9999
G30 P1 X10  Y320 Z-9999
G30 P2 X340 Y320 Z-9999
G30 P3 X340 Y10  Z-9999
G30 P4 X175 Y175 Z-9999 S4