
G91                     ; relative positioning
G1 H2 Z5 F15000          ; lift Z relative to current position
G1 H1 X-375 Y-375 F8000 ; move quickly to X or Y endstop and stop there (first pass)
G1 H1 X-375             ; home X axis
G1 H1 Y-375             ; home Y axis
G1 X5 Y5 F15000          ; go back a few mm
G1 H1 X-375 F360        ; move slowly to X axis endstop once more (second pass)
G1 H1 Y-375             ; then move slowly to Y axis endstop
G90                     ; absolute positioning