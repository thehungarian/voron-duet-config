G90  				; Absolute
G1 X79 Y0	F10000  ; Goto probe location
G91 				; Relative
G1 Z-350 H1 F50
; The endstop triggers this far from the bed 
G92 Z1.49
G90 				; Absolute
G1 Z5 F1000			; Go back up
