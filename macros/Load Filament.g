; macro
T0		; Select tool
M109 S200	; set the current hotend temperature to 200 and wait for it
G1 E2300 F3600	; extrude 640mm at heigh speed 
G1 E40 F200	; extrude 40mm at low speed
M84 P3 E0	; turn extruder motor off so the user can feed by hand - change for another tool
