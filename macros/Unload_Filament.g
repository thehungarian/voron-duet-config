;macro /macros/_Unload_filament.g
T0		;Select tool
;M109 S100	;set hotend temperature to 100 and wait for it – NOT USED as if hotend is hot, it wait for cooling which slow the process, hence heat temperature request shall be done manually before running this macro.
G1 E-5 F500	;Retract 5mm filament at low speed 
G1 E-2400 F3600	;Retract 680mm filament at high speed
M84 E0 		;turn extruder motor off to let user unload if filament locked - 
;T99		;deselect tool (useful ?)
